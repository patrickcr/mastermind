
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

 require('./bootstrap');

 window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

 Vue.component('example', require('./components/Example.vue'));

 export class Play
 {
 	constructor(){

 		this.sequence = [null,null, null, null];
 		this.status = [null,null, null, null];
 	}

 }

 export class Board
 {

 	constructor()
 	{
 		this.rows = [];
 		this.started = false;
 		this.info = {};
 		this.rowIndex = 9;
 		this.row = null;
 		this.colorIndex = 0;
 		this.canCheck = false;
 		this.win = null;

 	}
 	restart(){
 		this.started = false;
 		this.rowIndex = 9;
 		this.colorIndex = 0;
 		this.canCheck = false;
 		this.win = null;

 	}

 	start(name){
 		this.rows = [];
 		let data ={"name" : this.name };
 		axios.post('/start', data).then(response =>{
 			
 			this.info = response.data;

 			for(let i = 0; i <10; i++)
 				this.rows.push(new Play);

 			this.started = true;

 		}).catch(error => console.log("server not available"));

 		
 	}
 	select(rowIndex, colorIndex)
 	{
 		if(this.rowIndex == rowIndex)
 			this.colorIndex = colorIndex;
 		
 	}
 	play(color){

 		let play = this.rows[this.rowIndex];
 		play.sequence[this.colorIndex]  = color;
 		Vue.set(this.rows, this.rowIndex, play)


 		if(this.colorIndex < 3)
 			this.colorIndex +=1;
 		else
 			this.colorIndex =0;


 		let findEmptys = play.sequence.filter((color, index) =>{
 			return color == null;
 		})

 		//find in sequence if all is filled on if emptys wich means null is equals to 0
 		if(findEmptys.length == 0)
 			this.canCheck = true;
 		


 	}
 	check(){
 		//get the current play
 		let play = this.rows[this.rowIndex];
 		//create a data object to be set with the sequence
 		let data = {"sequence" : play.sequence};
 		//send the sequence to be validated 
 		axios.post('/play', data).then(result =>{

 			//set the current row result
 			Vue.set(this.rows, this.rowIndex, result.data)
 			//check the result of the play
 			if(!result.data.result)
 			{
 				//decrement the row index
 				this.rowIndex -=1;

 				//set color index to start  0
 				this.colorIndex =0;

				//disable the button can check
 				this.canCheck = false;

 				//if we arrive at end of array we finnish the game
 				if(this.rowIndex == -1)
 					this.win = false;

 			}
 			else
 			{
 				this.win = true;
 				this.canCheck = false;
 			}

 		}).catch(error => console.log("server not available"));
 	}
 }
 const app = new Vue({
 	el: '#app',
 	data:
 	{	
 		appStarted : false,
 		name: "",
 		board: new Board()
 	},
 	mounted(){

 		setTimeout(() => { this.appStarted = true}, 200)
 	}
 });
