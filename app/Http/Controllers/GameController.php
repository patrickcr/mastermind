<?php

namespace App\Http\Controllers;

use App\Game;
use Illuminate\Http\Request;

class GameController extends Controller
{
	public function index(){

		return view('index');
	}
	public function start(Request $request){
		//start a new game passing the name of the player
		$game = new Game($request->input('name'));
		//save the game in session
		$request->session()->put('game', $game);
		//return the game instance
		return json_encode($game);
	}

	public function play(Request $request)
	{
		if ($request->session()->exists('game')) {
			//get the game form the session
			$game = $request->session()->get('game');
			//execute the play and get the result of the board
			$play =$game->play($request->input('sequence'));
			//save the current game in the session
			$request->session()->put('game', $game);
			//return the result
			return json_encode($play);
		}
	}
}
